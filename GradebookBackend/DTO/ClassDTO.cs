﻿
namespace GradebookBackend.DTO
{
    public class ClassDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
